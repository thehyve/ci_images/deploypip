#!/usr/bin/env bash
NEWCONTAINER=$(buildah --storage-driver vfs from scratch)
SCRATCHMNT=$(buildah --storage-driver vfs mount ${NEWCONTAINER})
podman --cgroup-manager=cgroupfs --storage-driver vfs run -v ${SCRATCHMNT}:/mnt:rw --systemd=false fedora:latest dnf install --installroot /mnt bash coreutils python3 python3-pip --releasever ${RELEASE} --setopt=tsflags=nodocs --setopt=install_weak_deps=False --setopt=override_install_langs=en_US.utf8 -y
if [ -d ${SCRATCHMNT} ]; then rm -rf ${SCRATCHMNT}/var/cache/dnf; fi
buildah --storage-driver vfs config --label name=${CI_PROJECT_NAME} ${NEWCONTAINER}
buildah --storage-driver vfs config --cmd /bin/bash ${NEWCONTAINER}
buildah --storage-driver vfs unmount ${NEWCONTAINER}
buildah --storage-driver vfs commit ${NEWCONTAINER} ${CI_PROJECT_NAME}
buildah --storage-driver vfs tag ${CI_PROJECT_NAME} ${CI_REGISTRY_IMAGE}:${RELEASE}
buildah --storage-driver vfs push --creds ${CI_REGISTRY_USER}:${CI_JOB_TOKEN} ${CI_REGISTRY_IMAGE}:${RELEASE}
